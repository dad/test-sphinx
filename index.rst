.. toctree::
   :maxdepth: 2

  syntaxe_rest


Test de génération de documentation html
########################################

Bonjour voici une doc générée automatiquement par sphinx.
Le pipeline gitlab-ci.yml génère la doc au format HTML dans pages.

Bonne journée

** ça marche ! **

----

Fichier inclus:

.. include:: doc1.rst

**et je ressemble à ça** 

.. literalinclude:: doc1.rst

----

Exemples de syntaxe pour rst: 
#############################

.. include:: example.rst

.. literalinclude:: example.rst


